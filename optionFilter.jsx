import React,{Component} from 'react';
class OptionFilter extends Component{
    state={
        coursesArr:["ANGULAR","JAVASCRIPT","REACT","BOOTSTRAP","CSS","REST AND MICROSERVICES","NODE"],
    }
    handleChange=(e)=>{
        const {currentTarget:input}=e;
        let options={...this.props.options};
        options[input.name]=this.updateCBs(
            options[input.name],
            input.checked,
            input.value
        )
        this.props.onOptionsChange(options);
    };
    updateCBs=(inpValues,checked,value)=>{
        let inpArr=inpValues?inpValues.split(","):[];
        if(checked) inpArr.push(value);
        else{
            let index=inpArr.findIndex((ele)=>ele===value);
            if(index>=0)inpArr.splice(index,1);
        }
        return inpArr.join(",");
    };
    createCheck=(arr,values,name,label)=>{
        return <ul class="list-group">
                    <li class="list-group-item"><b>{label}</b></li>
                    {
                       arr.map((x)=>(
                            <li className="list-group-item form-check pl-5">
                                <input
                                className="form-check-input"
                                value={x}
                                id={x}
                                type="checkbox"
                                name={name}
                                checked={values.find((x1)=>x1===x)}
                                onChange={this.handleChange}
                                onOptionsChange={this.handleOptionChange}
                                />
                                <label className="form-check-label" htmlFor={x}>{x}</label>
                            </li>
                ))
            }
            </ul>
    }
    render(){
        let {course=""}=this.props.options
        let {coursesArr}=this.state;
        return(
        <div className="container">
            <div className="row">
                <div className="col-12">
                    {this.createCheck(coursesArr,course.split(","),"course","Option")}
                </div>
            </div>
        </div>
        )
    }
}
export default OptionFilter;