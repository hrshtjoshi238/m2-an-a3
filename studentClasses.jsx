import React,{Component} from 'react';
import http from '../services/httpService';
import auth from '../services/authservices';
class StudentClasses extends Component{
    state={
        classDetails:[]
    }
    async componentDidMount(){
        let user=auth.getUser()
        let response=await http.get(`/getStudentClass/${user.name}`)
        let {data}=response;
        console.log(data);
        this.setState({classDetails:data});
    }
    render(){
        let {classDetails=[]}=this.state;
        return(
            <div className="container" style={{marginTop:"30px"}}>
                <h3>All Scheduled Classes</h3>
                <table class="table">
                    <thead>
                      <tr className="bg-light">
                        <th scope="col">Course Name</th>
                        <th scope="col">Start Time</th>
                        <th scope="col">End Time</th>
                        <th scope="col">Faculty Name</th>
                        <th scope="col">Topic</th>
                      </tr>
                    </thead>
                    <tbody>
                        {
                            classDetails.map((x)=>{
                                return <tr className="bg-warning">
                                    <td scope>{x.course}</td>
                                    <td>{x.time}</td>
                                    <td>{x.endTime}</td>
                                    <td>{x.facultyName}</td>
                                    <td>{x.topic}</td>
                                </tr>
                            })
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}
export default StudentClasses;