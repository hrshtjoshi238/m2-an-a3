import React,{Component} from 'react';
import http from '../services/httpService';
import auth from '../services/authservices';
import ClassSchedule from './classSchedule';
class ScheduledClasses extends Component{
    state={
        classDetails:[],
        edit:false,
    }
    async componentDidMount(){
        let user=auth.getUser()
        let response=await http.get(`/getFacultyClass/${user.name}`)
        let {data}=response;
        console.log(data);
        this.setState({classDetails:data});
    }
    changeSchedule=(data)=>{
        this.setState({edit:true,currentData:data});
    }
    handleEdit=(data)=>{
        alert(data);
        this.setState({edit:false});
    }
    render(){
        let {classDetails=[],edit,currentData}=this.state;
        console.log(classDetails);
        return(
            <div className="container" style={{marginTop:"30px"}}>
                {
                !edit?(<React.Fragment>
                <h3>All Scheduled Classes</h3>
                <table class="table">
                    <thead>
                      <tr className="bg-light">
                        <th scope="col">Course Name</th>
                        <th scope="col">Start Time</th>
                        <th scope="col">End Time</th>
                        <th scope="col">Topic</th>
                        <th scope="col"></th>
                      </tr>
                    </thead>
                    <tbody>
                        {
                            classDetails.map((x)=>{
                                return <tr style={{background:"hsl(48, 100%, 50%)"}}>
                                    <td scope>{x.course}</td>
                                    <td>{x.time}</td>
                                    <td>{x.endTime}</td>
                                    <td>{x.topic}</td>
                                    <td className="p-2"><button className="btn btn-secondary" onClick={()=>this.changeSchedule(x)}>Edit</button></td>
                                </tr>
                            })
                        }
                    </tbody>
                </table>
                </React.Fragment>):<React.Fragment>
                    <ClassSchedule
                    handleEdit={this.handleEdit}
                    classData={currentData}
                    onEdit={"yes"}/>
                </React.Fragment>
                }
            </div>
        )
    }
}
export default ScheduledClasses;