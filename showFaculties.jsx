import React,{Component} from 'react';
import queryString from 'query-string';
import http from '../services/httpService';
import OptionFilter from './optionFilter';
class ShowFaculties extends Component{
    state={
        facultyDetails:{}
    }
    componentDidMount(){
        this.fetchData();
    }
    componentDidUpdate(prevProps,prevState){
        if(this.props!==prevProps){
            this.fetchData();
        }
    }
    async fetchData(){
        console.log("In Fetch");
        let queryParams=queryString.parse(this.props.location.search);
        //let queryParams=queryString.parse(this.props.location.search);
        let searchStr=this.makeSearchStr(queryParams);
        let response = await http.get(`/getFaculties?${searchStr}`);
        let {data}=response;
        console.log(data);
        this.setState({facultyDetails:data});
    }
    handleOptionChange=(options)=>{
        options.page="1";
        this.callURL("/allStudents",options);
    }
    callURL=(url,options)=>{
        let searchStr=this.makeSearchStr(options);
        this.props.history.push({
            pathName:url,
            search:searchStr
        });
    }
    makeSearchStr=(options)=>{
        let {page,course}=options;
        let searchStr="";
        searchStr=this.addToQueryString(searchStr,"course",course);
        searchStr=this.addToQueryString(searchStr,"page",page);
        return searchStr;
    };
    addToQueryString=(searchStr,name,value)=>{
        return value?
        searchStr?`${searchStr}&${name}=${value}`
        :`${name}=${value}`
        :searchStr;
    }
    updatePage=(num)=>{
        let queryParams=queryString.parse(this.props.location.search);
        let {page}=queryParams;
        let newPage="";
        if(num===0){
            newPage=parseInt(page)-1;
        }
        else{
            newPage=parseInt(page)+1;
        }
        queryParams.page=newPage;
        this.callURL("/allStudents",queryParams);
    }
    render(){
        let {facultyDetails={}}=this.state;
        let {items=[],page,totalItems,totalNum}=facultyDetails;
        let queryParams=queryString.parse(this.props.location.search);
        let startIndex=parseInt(page)*3-2;
        let endIndex=startIndex+parseInt(totalItems)-1;
        return(
            <div className="container" style={{marginTop:"30px"}}>
                <div className="row">
                    <div className="col-3">
                        <OptionFilter
                        options={queryParams}
                        onOptionsChange={this.handleOptionChange}
                        let />
                    </div>
                    <div className="col-9">
                        <h3>All Faculties</h3>
                        <span>{startIndex+"-"+endIndex+" of "+totalNum}</span>
                        <div className="row bg-light p-2">
                            <div className="col-4">Id</div>
                            <div className="col-4">Name</div>
                            <div className="col-4">Courses</div>
                        </div>
                        {
                            items.map((x,index)=>{
                                return <div className="row bg-warning border p-2" key={index}>
                                <div className="col-4">{x.id}</div>
                                <div className="col-4">{x.name}</div>
                                <div className="col-4">{x.courses.map(c=><React.Fragment>{c}<br/></React.Fragment>)}</div>
                                </div>  
                            })
                        }
                        <div className="row">
                            <div className="col-1">{page<=1?"":<button className="btn btn-secondary" onClick={()=>this.updatePage(0)}>Previous</button>}</div>
                            <div className="col-10"></div>
                            <div className="col-1">{totalItems<3?"":<button className="btn btn-secondary" onClick={()=>this.updatePage(1)}>Next</button>}</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default ShowFaculties;