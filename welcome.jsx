import React,{Component} from 'react';
import auth from '../services/authservices';
class Welcome extends Component{
    render(){
        let user=auth.getUser();
        return(
            <div className="component text-center" style={{marginTop:"40px"}}>
                <h3 className="text-warning text-center">Welcome to {user.role==="admin"?"Admin":user.role==="student"?"Student":"Faculty"} Portal</h3>
            </div>
        )
    }
}
export default Welcome;