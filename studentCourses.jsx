import React,{Component} from 'react';
import http from '../services/httpService';
import auth from '../services/authservices';
class StudentCourses extends Component{
    state={
        courseDetails:[]
    }
    async componentDidMount(){
        let user=auth.getUser()
        let response=await http.get(`/getStudentCourse/${user.name}`)
        let {data}=response;
        console.log(data);
        this.setState({courseDetails:data});
    }
    render(){
        let {courseDetails=[]}=this.state;
        return(
            <div className="container" style={{marginTop:"30px"}}>
                <h3>Courses Assigned</h3>
                <table class="table">
                    <thead>
                      <tr className="bg-light">
                        <th scope="col">Course Id</th>
                        <th scope="col">Course Name</th>
                        <th scope="col">Course Code</th>
                        <th scope="col">Description</th>
                      </tr>
                    </thead>
                    <tbody>
                        {
                            courseDetails.map((x)=>{
                                return <tr style={{background:"lightgreen"}}>
                                    <td scope>{x.courseId}</td>
                                    <td>{x.name}</td>
                                    <td>{x.code}</td>
                                    <td>{x.description}</td>
                                </tr>
                            })
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}
export default StudentCourses;