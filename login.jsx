import React,{Component} from 'react';
import http from '../services/httpService';
import auth from '../services/authservices';
class Login extends Component{
    state={
        form:{
            email:"",
            password:"",
        },
        errors:{},
    }
    handleChange =(e)=>{
        const {currentTarget:input}=e;
        let s1={...this.state};
        if(input.name=="password"){
            s1.errors.password=this.validatePassword(input.value);
        }
        s1.form[input.name]=input.value;
        this.setState(s1);
    };
    handleSubmit = (e) =>{
        e.preventDefault();
        this.loginUser("/login",this.state.form);
    }
    validatePassword=(password)=>{
        return password.length<7?"Password must have 7 characters":"";
    }
    isError=(errors)=>{
        let keys=Object.keys(errors);
        return keys.reduce((acc,curr)=>errors[curr]?++acc:acc,0);
    }
    async loginUser(url,obj){
        try {
            let response= await http.post(url,obj);
            let {data}=response;
            auth.login(data);
            window.location="/";
        }
        catch (ex){
            if(ex.response&&ex.response.status===500){
                let error=ex.response.data;
                this.setState({errors2:error});
            }
        }
    }
    render(){
        let {email,password}=this.state.form;
        let {errors,errors2}=this.state;
        return(
            <div className="container" style={{marginTop:"60px"}}>
                    <h3 className="text-center">Login</h3>
                    {errors2?<div className="text-danger text-center">Username or Password are invalid</div>:""}<br/>
                    <div className="form-group row">
                    <div className="col-3 text-center"><label htmlFor="name">Email<span className="text-danger">*</span></label></div>
                    <div className="col-6"><input className="form-control text-left"
                        type="email"
                        id="email"
                        name="email"
                        value={email}
                        onChange={this.handleChange}/>
                        <span>We'll Never share your email with anyone else</span>
                    </div>
                    </div>
                    <br/>
                    <div className="form-group row">
                    <div className="col-3 text-center"><label htmlFor="password">Password<span className="text-danger">*</span></label></div>
                    <div className="col-6"><input className="form-control text-left"
                        id="password"
                        type="password"
                        name="password"
                        value={password}
                        onChange={this.handleChange}
                        />
                        {
                            errors.password?<div style={{color:"rgb(204, 0, 102)",background:"rgb(255, 153, 204)",padding:"12px"}}>{errors.password}</div>:""
                        }
                    </div></div>
                <div className="col-12 text-center">
                    <button className="btn btn-primary" disabled={this.isError(errors)} onClick={this.handleSubmit}>Login</button>
                </div>
            </div>
        )
    }
}
export default Login;
