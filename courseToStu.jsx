import React,{Component} from 'react';
import http from '../services/httpService'
import EditCousrse from './editCouse';
class CourseToStu extends Component{
    state={
        courseDetails:[],
        studentsName:[],
        view:0,
        courseData:{},
    }
    componentDidMount(){
        this.fetchData();
    }
    componentDidUpdate(prevProps,prevState){
        if(this.props!==prevProps){
            this.fetchData();
        }
    }
    async fetchData(){
        //console.log("In Fetch");
        let response = await http.get(`/getCourses?`);
        let response2 = await http.get(`/getStudentNames`)
        let {data}=response;
        //console.log(response2.data);
        this.setState({courseDetails:data,studentsName:response2.data});
    }
    updateData=(course)=>{
        this.setState({courseData:course,view:1});
    }
    handleUpdate=()=>{
        this.putData('/putCourse',this.state.courseData);
    }
    async putData(url,obj){
        try {
            let response= await http.put(url,obj);
            let {data}=response;
            this.setState({view:0});
        }
        catch (ex){
            if(ex.response&&ex.response.status===500){
                ////console.log(ex.response.data)
                let error=ex.response.data;
                this.setState({errors2:error,view:0});
            }
        }
    }
    render(){
        let {courseDetails=[],studentsName=[],view,courseData}=this.state;
        //console.log(courseDetails);
        
        return(
            <div className="container" style={{marginTop:"30px"}}>
                <h3>Add students to a courses</h3>
                {
                    view===0?<React.Fragment>
                <div className="row bg-light p-2">
                    <div className="col-1">Course Id</div>
                    <div className="col-2">Name</div>
                    <div className="col-2">Course Code</div>
                    <div className="col-4">Description</div>
                    <div className="col-2">Students</div>
                    <div className="col-1"></div>
                </div>
                {
                    courseDetails.map((x)=> <div className="row bg-warning border p-2">
                    <div className="col-1">{x.courseId}</div>
                    <div className="col-2">{x.name}</div>
                    <div className="col-2">{x.code}</div>
                    <div className="col-4">{x.description}</div>
                    <div className="col-2">{x.students.map(c=><React.Fragment>{c}<br/></React.Fragment>)}</div>
                    <div className="col-1 p-2"><button className="btn btn-secondary btn-sm" onClick={()=>this.updateData(x)}>Edit</button></div>
                    </div>
                    )
                }
                </React.Fragment>:
                <React.Fragment>
                    <hr/>
                    <EditCousrse
                    use={"student"}
                    studentsName={studentsName}
                    courseData={courseData}
                    onOptionsChange={this.updateData}
                    onUpdateClick={this.handleUpdate}
                    />
                </React.Fragment>
                }
            </div>
        )
    }
}
export default CourseToStu;