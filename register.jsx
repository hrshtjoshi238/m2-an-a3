import React,{Component} from 'react';
import http from '../services/httpService';
class Register extends Component{
    state={
        personData:{
            name:"",
            email:"",
            password:"",
            role:"",
            password2:"",
        },
        errors:{},
        rolesArr:["Student","Faculty"],
    }
    handleChange=(e)=>{
        let {currentTarget:input}=e;
        let s1={...this.state};
        if(input.name=="password"){
            s1.errors.password=this.validatePassword(input.value);
        }
        if(input.name=="password2"){
            s1.errors.password2=this.validatePassword2(s1.personData.password,input.value);
        }
        if(input.name=="email"){
            s1.errors.email=this.validateEmail(input.value);
        }
        input.type=="radio"?s1.personData[input.name]=input.value.toLowerCase():
        s1.personData[input.name]=input.value;
        this.setState(s1);
    }
    validateAll=()=>{
        let {name,email,password,password2,role}=this.state.personData;
        let errors={};
        errors.name=this.validateName(name);
        errors.email=this.validateEmail(email);
        errors.password=this.validatePassword(password);
        errors.password2=this.validatePassword2(password,password2);
        errors.role=this.validateRole(role);
        return errors;
    }
    validateName=(name)=>!name?"Name is not entered":"";
    validatePassword2=(p1,p2)=>p1!=p2?"Password didn't match":"";
    validatePassword=(p)=>(
        !p?"Password is not entered"
        :p.length<7?"Password must be atleast 7 characters":""
    )
    validateEmail=(email)=>{
        return !email?"Email must be entered":
        email.indexOf("@")===-1?"Enter valid email":
        "";
    }
    validateRole=(role)=>!role?"Role is not selected":"";
    isFormError=()=>{
        let errors=this.validateAll();
        console.log(this.isError(errors));
        return this.isError(errors);
    }
    isError=(errors)=>{
        let keys=Object.keys(errors);
        return keys.reduce((acc,curr)=>errors[curr]?++acc:acc,0);
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        let errors=this.validateAll();
        if(this.isError(errors)){
            let s1={...this.state};
            s1.errors=errors;
            this.setState(s1);
        }
        else{
            this.registerEmp("/register",this.state.personData);
        }
    }
    async registerEmp(url,obj){
        try {
            let response= await http.post(url,obj);
            let {data}=response;
            alert("User created successfully");
            window.location="/customer";
        }
        catch (ex){
            if(ex.response&&ex.response.status===500){
                //console.log(ex.response.data)
                let error=ex.response.data;
                this.setState({errors2:error});
            }
        }
    }
    getText=(type,name,value,label,placeholder,error)=>{
        return <React.Fragment>
            <label htmlFor={name}>{label}<span className="text-danger">*</span></label>
            <input
            className="form-control"
            id={name}
            type={type}
            name={name}
            placeholder={placeholder}
            value={value}
            onChange={this.handleChange}/>
            {
                error?<div style={{color:"rgb(204, 0, 102)",background:"rgb(255, 153, 204)",padding:"12px"}}>{error}</div>:""
            }
        </React.Fragment>
    }
    getRadio=(arr,name,value,label,error)=>{
        return <React.Fragment>
                    <label>{label}<span className="text-danger">*</span></label>
                    {
                        arr.map((x,index)=>{
                            return <div className="form-group">
                                <input
                                className="form-check-inline"
                                type="radio"
                                key={index}
                                name={name}
                                value={x}
                                id={x}
                                checked={value===x.toLowerCase()}
                                onChange={this.handleChange}/>
                                <label className="form-check-label" htmlFor={x}>{x}</label>
                            </div>       
                         })
                    }
                    {
                        error?<div style={{color:"rgb(204, 0, 102)",background:"rgb(255, 153, 204)",padding:"12px"}}>{error}</div>:""
                    }
            </React.Fragment>
    } 
    render(){
        let {name,email,password,password2,role}=this.state.personData;
        let {rolesArr,errors}=this.state;
          return(
            <div className="container mt-3">
                <h3>Register</h3>
                <div className="form-group">
                    {this.getText("text","name",name,"Name","Enter Name",errors.name)}
                </div>
                <div className="form-group">
                    {this.getText("password","password",password,"Password","Enter Password",errors.password)}
                </div>
                <div className="form-group">
                    {this.getText("password","password2",password2,"Confirm Password","Enter Password Again",errors.password2)}
                </div>
                <div className="form-group">
                    {this.getText("email","email",email,"Email","Enter Email",errors.email)}
                </div>
                <div className="form-group">
                    {this.getRadio(rolesArr,"role",role,"Role",errors.role)}
                </div>
                <div className="form-group">
                    <button className="btn btn-primary"
                    onClick={this.handleSubmit} disabled={this.isFormError()?"true":""}>Submit</button>
                </div>
            </div>
        ) 
    }
}
export default Register;