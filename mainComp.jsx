import React,{Component} from 'react';
import auth from '../services/authservices';
import {Route,Redirect,Switch} from 'react-router-dom';
import NavBar from './navBar';
import Logout from './logout';
import Login from './login';
import Welcome from './welcome';
import Register from './register';
import ShowStudents from './showStudents';
import ShowFaculties from './showFaculties';
import CourseToStu from './courseToStu';
import CourseToFac from './courseToFac';
import StudentClasses from './studentClasses';
import StudentCourses from './studentCourses';
import StudentDetails from './studentDetails';
import FacultyCourse from './facultyCourse';
import ScheduledClasses from './scheduledClasses';
import ClassSchedule from './classSchedule';
class MainComp extends Component{
    render(){
        const user=auth.getUser();
        return(
            <React.Fragment>
                <NavBar user={user}/>
                 <Switch>
                 {user &&user.role==="admin"?<Route path="/admin" component={Welcome}/>
                    :user &&user.role==="student"?<Route path="/student" component={Welcome}/>
                    :<Route path="/faculty" component={Welcome}/>
                }
                 <Route path="/allClasses" render={(props)=><StudentClasses {...props}/>}/>
                 <Route path="/allCourses" render={(props)=><StudentCourses {...props}/>}/>
                 <Route path="/coursesAssigned" render={(props)=><FacultyCourse {...props}/>}/>
                 <Route path="/scheduleClass" render={(props)=><ClassSchedule {...props}/>}/>
                 <Route path="/scheduledClass" render={(props)=><ScheduledClasses {...props}/>}/>
                 <Route path="/register" render={(props)=><Register/>}/>
                 <Route path="/studentCourse" component={CourseToStu}/>
                 <Route path="/studentDetails" render={(props)=><StudentDetails {...props}/>}/>
                 <Route path="/facultyCourse" component={CourseToFac}/>
                 <Route path="/allStudents" render={(props)=><ShowStudents {...props}/>}/>
                 <Route path="/allFaculties" render={(props)=><ShowFaculties {...props}/>}/>
                 <Route path="/logout" component={Logout}/>   
                 <Route path="/" render={(props)=>user?user.role==="admin"?<Redirect to="/admin"/>:user.role==="student"?<Redirect to="/student"/>:<Redirect to="/faculty"/>:<Login {...props}/>}/>
                 </Switch>
            </React.Fragment>
        )  
    }
}
export default MainComp;
