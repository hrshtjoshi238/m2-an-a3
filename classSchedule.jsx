import React,{Component} from 'react';
import http from '../services/httpService';
import auth from '../services/authservices';
class ClassSchedule extends Component{
    state={
        classData:{
            course:"",
            time:"",
            endTime:"",
            topic:"",
            facultyName:auth.getUser().name
        },
        courses:[],
        errors:{},
    }
    handleChange=(e)=>{
        let {currentTarget:input}=e;
        let s1={...this.state};
        s1.classData[input.name]=input.value;
        this.setState(s1);
    }
    getDropDwn=(arr,name,value,disabledText,error)=>{
        return <React.Fragment><select className="form-control" name={name} id={name} value={value} onChange={this.handleChange}>
            <option disabled selected value="">{disabledText}</option>
            {
                arr.map((x,index)=><option key={index}>{x}</option>)
            }
        </select>
        {error?<div style={{color:"rgb(204, 0, 102)",background:"rgb(255, 153, 204)",padding:"12px"}}>{error}</div>:""}</React.Fragment>
        
    }
    async componentDidMount(){
        let {classData,onEdit=""}=this.props
        //console.log(onEdit);
        let response=await http.get(`/getFacultyCourse/${auth.getUser().name}`)
        let {data}=response;
        let courses=data.map(x=>x.name);
        onEdit?this.setState({courses:courses,classData:classData}):this.setState({courses:courses});
    }
    getTime=(name,value,label,error)=>{
        return <React.Fragment>
            <label htmlFor={name}>{label}<span className="text-danger">*</span></label>
            <input
            type="time"
            className="form-control"
            name={name}
            id={name}
            value={value}
            onChange={this.handleChange}/>
            {
                error?<div style={{color:"rgb(204, 0, 102)",background:"rgb(255, 153, 204)",padding:"12px"}}>{error}</div>:""
            }
        </React.Fragment>
    }
    getText=(name,value,label,placeholder,error)=>{
        return <React.Fragment>
            <label htmlFor={name}>{label}<span className="text-danger">*</span></label>
            <input
            className="form-control"
            id={name}
            type="text"
            name={name}
            placeholder={placeholder}
            value={value}
            onChange={this.handleChange}/>
            {
                error?<div style={{color:"rgb(204, 0, 102)",background:"rgb(255, 153, 204)",padding:"12px"}}>{error}</div>:""
            }
        </React.Fragment>
    }
    validateAll=()=>{
        let {course,time,endTime,topic}=this.state.classData;
        let errors={};
        errors.course=this.validateCourse(course);
        errors.time=this.validateTime(time);
        errors.endTime=this.validateEndTime(endTime);
        errors.topic=this.validateTopic(topic);
        return errors;
    }
    validateCourse=(course)=>{
        return !course?"Course is not selected":"";
    }
    validateTime=(time)=>{
        return !time?"Time is not selected":"";
    }
    validateEndTime=(time)=>{
        return !time?"Time is not selected":"";
    }
    validateTopic=(topic)=>{
        return !topic?"Topic is not entered":"";
    }
    isError=(errors)=>{
        let keys=Object.keys(errors);
        return keys.reduce((acc,curr)=>errors[curr]?++acc:acc,0);
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        let {classData,onEdit=""}=this.props
        let errors=this.validateAll();
        if(this.isError(errors)){
            let s1={...this.state};
            s1.errors=errors;
            this.setState(s1);
        }
        else{
            !onEdit?this.addClass("/postClass",this.state.classData):this.editClass(`/postClass/${classData.classId}`);
        }
    }
    async addClass(url,obj){
        try {
            let response= await http.post(url,obj);
            let {data}=response;
            console.log(data);
            alert("Class schedule successfully");
            window.location="/";
        }
        catch (ex){
            if(ex.response&&ex.response.status===500){
                //console.log(ex.response.data)
                let error=ex.response.data;
                this.setState({errors2:error});
            }
        }
    }
    async editClass(url,obj){
        try {
            let response= await http.put(url,obj);
            let {data}=response;
            this.props.handleEdit(data);
        }
        catch (ex){
            if(ex.response&&ex.response.status===500){
                //console.log(ex.response.data)
                let error=ex.response.data;
                this.setState({errors2:error});
            }
        }
    }
    render(){
        let {courses=[],classData,errors}=this.state;
        let {course,time,endTime,topic}=classData;
        console.log(courses);
        return(
            <div className="container" style={{marginTop:"30px"}}>
                <h3>Schedule a class</h3>
                <div className="form-group">
                    {this.getDropDwn(courses,"course",course,'Select Course',errors.course)}
                </div>
                <div className="form-group">
                    {this.getTime("time",time,'Time',errors.time)}
                </div>
                <div className="form-group">
                    {this.getTime("endTime",endTime,'End Time',errors.endTime)}
                </div>
                <div className="form-group">
                    {this.getText("topic",topic,'Topic','Enter Topic',errors.topic)}
                </div>
                <div className="form-group">
                    <button className="btn btn-primary" onClick={this.handleSubmit}>Get Scheduled</button>
                </div>
            </div>
        )
    }
}
export default ClassSchedule;