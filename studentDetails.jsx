import React,{Component} from 'react';
import http from '../services/httpService';
import auth from '../services/authservices';
class StudentDetails extends Component{
    state={
        genderList:["Male","Female"],
        stuDetails:{
            name:auth.getUser().name,
            dob:{
                day:"",
                month:"",
                year:"",
            },
            gender:"",
            about:"",
        },
        months:["January","February","March","April","May","June","July","August","September","October","November","December"],
        errors:{} 
    }
    handleChange=(e)=>{
        let {currentTarget:input}=e;
        let s1={...this.state};
        input.className.split(" ")[1]==="dob"?
            s1.stuDetails[input.className.split(" ")[1]][input.name]=input.value
            :s1.stuDetails[input.name]=input.value;
        this.setState(s1);
    }
    validateAll=()=>{
        let {gender,dob}=this.state.stuDetails;
        let errors={};
        errors.gender=this.validateGender(gender);
        errors.dob=this.validateDOB(dob);
        return errors;
    }
    validateGender=(gender)=>{
        return !gender?"Gender is not selected":""
    }
    validateDOB=(dob)=>{
        return !dob.day||!dob.month||!dob.year?"Date is not selected":"";
    }
    isFormError=()=>{
        let errors=this.validateAll();
        //console.log(this.isError(errors));
        return this.isError(errors);
    }
    isError=(errors)=>{
        let keys=Object.keys(errors);
        return keys.reduce((acc,curr)=>errors[curr]?++acc:acc,0);
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        let errors=this.validateAll();
        if(this.isError(errors)){
            let s1={...this.state};
            s1.errors=errors;
            this.setState(s1);
        }
        else{
            let s1={...this.state};
            let details={
                gender:s1.stuDetails.gender,
                name:s1.stuDetails.name,
                dob:s1.stuDetails.dob.day+"-"+s1.stuDetails.dob.month+"-"+s1.stuDetails.dob.year,
                about:s1.stuDetails.about
            };
            this.addToDetails('/postStudentDetails',details);
        }
    }
    async addToDetails(url,obj){
        console.log(obj);
        try {
            let response= await http.post(url,obj);
            let {data}=response;
            alert(data);
            window.location="/";
        }
        catch (ex){
            if(ex.response&&ex.response.status===500){
                let error=ex.response.data;
                this.setState({errors2:error});
            }
        }
    }
    async componentDidMount(){
        let user=auth.getUser();
        let response=await http.get(`/getStudentDetails/${user.name}`);
        let {data}=response;
        console.log(data);
        if(data.dob&&data.gender&&data.about){
            let details={
            name:data.name,
            gender:data.gender,
            about:data.about,
            dob:{
                day:data.dob.split("-")[0],
                month:data.dob.split("-")[1],
                year:data.dob.split("-")[2],
            }
            };
            this.setState({
                stuDetails:details,
                detailsAvail:true
            });
        }
        else{
            this.setState({detailsAvail:false});
        }
    }
    getYears=()=>{
        let years=[];
        for(let i=1980;i<=2021;i++){
            years.push(i);
        }
        return years;
    }
    getDays=(year,month)=>{
        let {months}=this.state;
        let days=[];
        let leap=false;
        if(month!="February"){
            if(month==="July"||month==="August"){
                for(let i=1;i<=31;i++){
                    days.push(i);
                }
            }
            else{
                let index=months.findIndex((x)=>x===month);
                if(index<=5){
                    if ((index+1)%2!=0){
                        for(let i=1;i<=31;i++){
                            days.push(i);
                        }
                    }
                    else{
                        for(let i=1;i<=30;i++){
                            days.push(i);
                        }
                    }
                }
                else if(index>7){
                    if ((index+1)%2!=0){
                        for(let i=1;i<=30;i++){
                            days.push(i);
                        }
                    }
                    else{
                        for(let i=1;i<=31;i++){
                            days.push(i);
                        }
                    }
                }
            }
        }
        else{
            if(parseInt(year)%400===0){
                leap=true;
            }
            else if(parseInt(year)%100===0){
                leap=false;
            }
            else if(parseInt(year)%4===0){
                leap=true;
            }
            else{
                leap=false;
            }
            if(leap===false){
                for(let i=1;i<=28;i++){
                    days.push(i);
                }
            }
            else{
                for(let i=1;i<=29;i++){
                    days.push(i);
                }
            }
        }
        return days;
    }
    render(){
        let {genderList,stuDetails={},errors,months,detailsAvail,errors2}=this.state;
        let {gender,about,dob}=stuDetails;
        let years=this.getYears();
        let days=this.getDays(dob.year,dob.month);
        return(
            <div className="container" style={{marginTop:"30px"}}>
                <h3>Student Details</h3>
                <span className="text-danger text-center">{errors2}</span>
                <div className="row">
                    <div className="col-2">Gender<span className="text-danger">*</span></div>
                    {
                        genderList.map((x,index)=>(
                            <div className="col-3">
                                <input
                                key={index}
                                className="form-checked"
                                type="radio"
                                name="gender"
                                value={x}
                                id={x}
                                checked={gender.toLowerCase()===x.toLowerCase()}
                                onChange={this.handleChange}/>
                                <label className="form-checked-label" htmlFor={x}>{x}</label>
                            </div>
                        ))
                    }
                    {errors.gender?<div className="col-12 form-group" style={{color:"rgb(204, 0, 102)",background:"rgb(255, 153, 204)",padding:"12px"}}>{errors.gender}</div>:""}
                </div>
                <div className="form-group">
                    <label>Date of Birth<span className="text-danger">*</span></label>
                        <div className="row">
                            <div className="col-4 form-group">
                                <select className="form-control dob" name="year" value={dob.year} onChange={this.handleChange}>
                                    <option disabled selected value="">Select Year</option>
                                    {
                                        years.map((x,index)=><option key={index}>{x}</option>)
                                    }
                                </select>
                            </div>
                            <div className="col-4 form-group">
                                <select className="form-control dob" name="month" value={dob.month} onChange={this.handleChange}>
                                    <option disabled selected value="">Select Month</option>
                                    {
                                        months.map((x,index)=><option key={index}>{x}</option>)
                                    }
                                </select>
                            </div>
                            <div className="col-4 form-group">
                                <select className="form-control dob" name="day" value={dob.day} onChange={this.handleChange}>
                                    <option disabled selected value="">Select Day</option>
                                    {
                                        days.map((x,index)=><option key={index}>{x}</option>)
                                    }
                                </select>
                            </div>
                            {errors.dob?<div className="col-12 form-group" style={{color:"rgb(204, 0, 102)",background:"rgb(255, 153, 204)",padding:"12px"}}>{errors.dob}</div>:""}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="about">About</label>
                        <textarea class="form-control" id="about" name="about" value={about} onChange={this.handleChange} rows="3"/>
                    </div>
                    {detailsAvail?"":<div className="form-group">
                        <button className="btn btn-primary" onClick={this.handleSubmit}>Add Details</button>
                    </div>}
            </div>
        )
    }
}
export default StudentDetails; 