import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {Navbar,Nav,NavDropdown} from 'react-bootstrap';
class NavBar extends Component{
    render(){
        const {user}=this.props;
        return(
            <Navbar collapseOnSelect expand="lg" bg="success" variant="light">
            <Link className="navbar-brand" to="/">Home</Link>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="mr-auto">
                {
                    user&&user.role=="admin"&&<Link className="nav-link" to="/register">Register</Link>
                }
                {
                    user&&user.role=="admin"&&<NavDropdown title="Assign" id="collasible-nav-dropdown">
                        <NavDropdown.Item><Link className="nav-link text-dark" to="/studentCourse">Student to Course</Link></NavDropdown.Item>
                        <NavDropdown.Item><Link className="nav-link text-dark" to="/facultyCourse">Faculty to Course</Link></NavDropdown.Item>
                    </NavDropdown>
                }
                {
                    user&&user.role=="admin"&&<NavDropdown title="View" id="collasible-nav-dropdown">
                        <NavDropdown.Item><Link className="nav-link text-dark" to="/allStudents?page=1">All Students</Link></NavDropdown.Item>
                        <NavDropdown.Item><Link className="nav-link text-dark" to="/allFaculties?page=1">All Faculties</Link></NavDropdown.Item>
                    </NavDropdown>
                }
                {
                    user&&user.role=="student"&&<Link className="nav-link" to="/studentDetails">Student Details</Link>
                }
                {
                    user&&user.role=="student"&&<Link className="nav-link" to="/allClasses">All Classes</Link>
                }
                {
                    user&&user.role=="student"&&<Link className="nav-link" to="/allCourses">All Course</Link>
                }
                {
                    user&&user.role=="faculty"&&<Link className="nav-link" to="/coursesAssigned">Courses</Link>
                }
                {
                    user&&user.role=="faculty"&&<NavDropdown title="View" id="collasible-nav-dropdown">
                        <NavDropdown.Item><Link className="nav-link text-dark" to="/scheduleClass">Schedule a class</Link></NavDropdown.Item>
                        <NavDropdown.Item><Link className="nav-link text-dark" to="/scheduledClass">All Scheduled Classes</Link></NavDropdown.Item>
                    </NavDropdown>
                }
              </Nav>
              <Nav className="ml-auto">
                  {
                      user&&<Link className="nav-link" to="">Welcome {user.role=="admin"?"Admin":user.name}</Link>
                  }
                  {
                    !user&&<Link className="nav-link" to="/">Login</Link>
                  }
                  {
                      user&&<Link className="nav-link" to="/logout">Logout</Link>
                  }
              </Nav>
            </Navbar.Collapse>
        </Navbar>
        )
    }
}
export default NavBar;