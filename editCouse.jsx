import React,{Component} from 'react';
class EditCousrse extends Component{
    handleChange=(e)=>{
        const {currentTarget:input}=e;
        let options={...this.props.courseData};
        options[input.name]=this.updateCBs(
            options[input.name],
            input.checked,
            input.value
        )
        this.props.onOptionsChange(options);
    };
    updateCBs=(inpValues,checked,value)=>{
        let inpArr=inpValues.length?inpValues:[];
        if(checked) inpArr.push(value);
        else{
            let index=inpArr.findIndex((ele)=>ele===value);
            if(index>=0)inpArr.splice(index,1);
        }
        console.log(inpArr);
        return inpArr;
    };
    getText=(name,value,label,placeholder,error)=>{
        return <React.Fragment>
            <label htmlFor={name}>{label}</label>
            <input
            className="form-control"
            id={name}
            type="text"
            disabled={"true"}
            name={name}
            placeholder={placeholder}
            value={value}
            onChange={this.handleChange}/>
            {
                error?<div style={{color:"rgb(204, 0, 102)",background:"rgb(255, 153, 204)",padding:"12px"}}>{error}</div>:""
            }
        </React.Fragment>
    }
    getCheck=(arr,values,name,label)=>{
        return <React.Fragment>
                    <label>{label}<span className="text-danger">*</span></label>
                    {
                       arr.map((x)=>(
                            <div className="form-group">
                                <input
                                className="form-check-inline"
                                value={x}
                                id={x}
                                type="checkbox"
                                name={name}
                                checked={values.find((x1)=>x1===x)}
                                onChange={this.handleChange}
                                onOptionsChange={this.handleOptionChange}
                                />
                                <label className="form-check-label" htmlFor={x}>{x}</label>
                            </div>
                ))
            }
            </React.Fragment>
    }
    render(){
        let {courseData,studentsName,facultiesName,use}=this.props;
        return(
            <div className="container">
                <h5>Edit the Course</h5>
                <div className="form-group">
                    {this.getText("name",courseData.name,"Name")}
                </div>
                <div className="form-group">
                    {this.getText("code",courseData.code,"Course Code")}
                </div>
                <div className="form-group">
                    {this.getText("description",courseData.description,"Course Description")}
                </div>
                <div className="form-group">
                    {use==="student"?this.getCheck(studentsName,courseData.students,"students","Students")
                    :this.getCheck(facultiesName,courseData.faculty,"faculty","Faculties")
                    }
                </div>
                <div className="ford-group">
                    <button className="btn btn-primary btn-sm" onClick={()=>this.props.onUpdateClick()}>Update</button>
                </div>
            </div>
        )
    }
}
export default EditCousrse;