const keyname = "user";
function login(obj){
    let str=JSON.stringify(obj);
    localStorage.setItem(keyname,str);
}
function logout(){
    let str=localStorage.removeItem(keyname);
    let obj=str?JSON.parse(str):null;
    return obj;
}
function getUser(){
    let str=localStorage.getItem(keyname);
    let obj=str?JSON.parse(str):null;
    return obj;
}
export  default {
    login,
    logout,
    getUser
};